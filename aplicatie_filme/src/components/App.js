import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './App.css';
import Login from './Login'

class App extends Component {
  render() {
    return (
        <div className="App">
          <div className="container">
           <Login />
          </div>  
        </div>
      
    );
  }
}

export default App;
