import React, { Component } from 'react';
// import TodoItem from './TodoItem';
// import PropTypes from 'prop-types';
import './App.css';

class App extends Component {
  
  constructor(props){
    super(props);
    this.state={
      username:'',
      password:''
    }
    this.login = this.login.bind(this);
    this.onChange = this.onChange.bind(this);
  }
  
  
  login(){
    console.log("ceva");
  }
  
  onChange(e){
    this.setState({[e.target.name]: e.target.value});
   
  }
  
  
  render() {
    return (
      <div className="row small-up-2 medium-up-3 large-up-4">
        <div className="Login">
        <h2>Login Page</h2>
        <label>Username</label>
        <input type="text" name="username" placeholder="username" onChange={this.onChange}/>
        <label>Parola</label>
        <input type="password" name="parola" placeholder="parola"  onChange={this.onChange}/>
        <input type="submit" value="Login" className="button" onClick={this.login}/>
      
         </div>
       </div>
      
    );
  }
}

export default App;
