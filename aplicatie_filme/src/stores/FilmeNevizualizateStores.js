import axios from 'axios'
import {EventEmitter} from 'fbemitter'

const SERVER = 'https://myworkspace-catipuiu16.c9users.io'

class FilmeNevizualizateStores{
  constructor(){
    this.content = []
    this.emitter = new EventEmitter()
  }
  async getAll(utilizatorId, filmeId){
    try {
      let response = await axios(`${SERVER}/utilizator/${utilizatorId}/filme/${filmeId}/filmenevizualizate/`)
      this.content = response.data
      this.emitter.emit('GET_ALL_SUCCESS')
    } catch (e) {
      console.warn(e)
      this.emitter.emit('GET_ALL_ERROR')
    }
  }
  async addOne(filme, utilizatorId, filmeId){
    try {
      await axios.post(`${SERVER}/utilizator/${utilizatorId}/filme/${filmeId}/filmenevizualizate/`, filme)
      this.emitter.emit('ADD_SUCCESS')
      this.getAll()
    } catch (e) {
      console.warn(e)
      this.emitter.emit('ADD_ERROR')
    }
  }

 
}

export default FilmeNevizualizateStores