import axios from 'axios'
import {EventEmitter} from 'fbemitter'

const SERVER = 'https://myworkspace-catipuiu16.c9users.io'

class FilmeStores{
  constructor(){
    this.content = []
    this.emitter = new EventEmitter()
  }
  async getAll(){
    try {
      let response = await axios(`${SERVER}/filme`)
      this.content = response.data
      this.emitter.emit('GET_ALL_SUCCESS')
    } catch (e) {
      console.warn(e)
      this.emitter.emit('GET_ALL_ERROR')
    }
  }
  async addOne(filme){
    try {
      await axios.post(`${SERVER}/filme`, filme)
      this.emitter.emit('ADD_SUCCESS')
      this.getAll()
    } catch (e) {
      console.warn(e)
      this.emitter.emit('ADD_ERROR')
    }
  }

 
}

export default FilmeStores