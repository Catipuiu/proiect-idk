import axios from 'axios'
import {EventEmitter} from 'fbemitter'

const SERVER = 'https://myworkspace-catipuiu16.c9users.io'

class UtilizatorStores{
  constructor(){
    this.content = []
    this.emitter = new EventEmitter()
  }
  async getAll(){
    try {
      let response = await axios(`${SERVER}/utilizator`)
      this.content = response.data
      this.emitter.emit('GET_ALL_SUCCESS')
    } catch (e) {
      console.warn(e)
      this.emitter.emit('GET_ALL_ERROR')
    }
  }
  async addOne(utilizator){
    try {
      await axios.post(`${SERVER}/utilizator`, utilizator)
      this.emitter.emit('ADD_SUCCESS')
      this.getAll()
    } catch (e) {
      console.warn(e)
      this.emitter.emit('ADD_ERROR')
    }
  }
  async deleteOne(id){
    try {
      await axios.delete(`${SERVER}/utilizator/${id}`)
      this.emitter.emit('DELETE_SUCCESS')
      this.getAll()
    } catch (e) {
      console.warn(e)
      this.emitter.emit('DELETE_ERROR')
    }
  }
 
}

export default UtilizatorStores