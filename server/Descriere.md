Proiectul ales de noi, echipa IDK consta intr-un Manager de filme favorite integrat cu TMDB.
Modul in care noi vrem sa il implementam este urmatorul:
1.Utilizatorul se va loga cu un username si o parola.

2.Utilizatorul dupa ce s-a logat va vedea o casuta de cautare.(legatura cu TMDB)

3.Dupa ce va cauta un film, utilizatorul il poate adauga la filme vizualizate/pe care doreste sa le vizualizeze.(legatura cu TMDB)

4.La sfarsit utilizatorul isi va putea vizualiza listele de filme.