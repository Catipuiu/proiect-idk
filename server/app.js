'use strict'
const express = require('express')
const bodyParser = require('body-parser')
const Sequelize = require('sequelize')
const Op = Sequelize.Op

const sequelize = new Sequelize('idk','root','',{
    dialect : 'mysql',
    define : {
        timestamps : false
    }
})

const Utilizator = sequelize.define('utilizator', {
    name :{
        type : Sequelize.STRING,
        allowNull : false,
        validate : { 
            len : [3, 50]
        }
    },
    parola :{
        type : Sequelize.STRING,
        allowNull : false,
        validate : { 
            len : [3, 20]
        }
    },
    username : {
        type : Sequelize.STRING,
        allowNull : false,
        validate : { 
           len : [3, 20]
        }
    },
  
})

const Filme = sequelize.define('filme', {
    name :{
        type : Sequelize.STRING,
        allowNull : false,
        validate : { 
            len : [3, 50]
        }
    },
     director :{
        type : Sequelize.STRING,
        allowNull : false,
        validate : { 
            len : [3, 50]
        }
    },
    gen : {
        type : Sequelize.STRING,
        allowNull : false,
        validate : { 
           len : [3, 20]
        }
    },
     lansare :{
        type : Sequelize.DATEONLY,
        allowNull : false,
    },
    
     descriere :{
        type : Sequelize.STRING,
        allowNull : false,
        validate : { 
            len : [3, 500]
        }
    },
    
     nota:{
        type : Sequelize.FLOAT,
        allowNull : false,
    },
  
})

const FilmeVizualizate = sequelize.define('filmevizualizate', {
     data:{
        type : Sequelize.DATEONLY,
        allowNull : false,
    },
  
})


const FilmeNevizualizate = sequelize.define('filmenevizualizate', {
     data:{
        type : Sequelize.DATEONLY,
        allowNull : false,
    },
  
})

FilmeVizualizate.belongsTo(Utilizator);
FilmeVizualizate.belongsTo(Filme);
FilmeNevizualizate.belongsTo(Utilizator);
FilmeNevizualizate.belongsTo(Filme);


const app = express()
app.use(express.static('../aplicatie_filme/build'))
app.use(bodyParser.json())

//sequelize.sync({force: true}).then(()=>{
 //   console.log('Databases create successfully')
//})


//PENTRU UTILIZATOR

//get pentru a crea tabelele
app.get('/create', async (req, res) => {
    try{
        await sequelize.sync({force : true})
        res.status(201).json({message : 'created'})
    }
    catch(e){
        console.warn(e)
        res.status(500).json({message : 'server error'})
    }
})

// pt a pune in tabele
app.post('/utilizator', async (req, res) => {
    try{
        if (req.query.bulk && req.query.bulk == 'on'){
            await Utilizator.bulkCreate(req.body)
            res.status(201).json({message : 'created'})
        }
        else{
            await Utilizator.create(req.body)
            res.status(201).json({message : 'created'})
        }
    }
    catch(e){
        console.warn(e)
        res.status(500).json({message : 'server error'})
    }
})



//pentru UTILIZATOR

app.get('/utilizator', async (req, res) => {
    try{
        let utilizator = await Utilizator.findAll()
        res.status(200).json(utilizator)
    }
    catch(e){
        console.warn(e)
        res.status(500).json({message : 'server error'})
    }
})


app.put('/utilizator/:id', async (req, res) => {
    try{
        let utilizator = await Utilizator.findById(req.params.id)
        if (utilizator){
            await utilizator.update(req.body)
            res.status(202).json({message : 'accepted'})
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    }
    catch(e){
        console.warn(e)
        res.status(500).json({message : 'server error'})
    }
})



app.delete('/utilizator/:id', async (req, res) => {
    try{
        let utilizator = await Utilizator.findById(req.params.id)
        if (utilizator){
            await utilizator.destroy()
            res.status(202).json({message : 'accepted'})
        }
        else{
            res.status(404).json({message : 'not found'})
        }
    }
    catch(e){
        console.warn(e)
        res.status(500).json({message : 'server error'})
    }
})






//PENTRU FILME

app.post('/filme', async (req, res) => {
    try{
        if (req.query.bulk && req.query.bulk == 'on'){
            await Filme.bulkCreate(req.body)
            res.status(201).json({message : 'created'})
        }
        else{
            await Filme.create(req.body)
            res.status(201).json({message : 'created'})
        }
    }
    catch(e){
        console.warn(e)
        res.status(500).json({message : 'server error'})
    }
})

app.get('/filme', async (req, res) => {
    try{
        let filme = await Filme.findAll()
        res.status(200).json(filme)
    }
    catch(e){
        console.warn(e)
        res.status(500).json({message : 'server error'})
    }
})






//PENTRU FILME VIZUALIZATE

/*
app.post('/filmevizualizate', async (req, res) => {
    try{
        if (req.query.bulk && req.query.bulk == 'on'){
            await FilmeVizualizate.bulkCreate(req.body)
            res.status(201).json({message : 'created'})
        }
        else{
            await FilmeVizualizate.create(req.body)
            res.status(201).json({message : 'created'})
        }
    }
    catch(e){
        console.warn(e)
        res.status(500).json({message : 'server error'})
    }
})
*/

app.post('/utilizator/:id1/filme/:id2/filmevizualizate/', async (req, res) => {
	try {
		let filme = await Filme.findById(req.params.id2)
		let utilizator = await Utilizator.findById(req.params.id1)
	
		if (!utilizator && !filme){
			res.status(404).send({message : 'not found'})
		}
		else{
		    let filmevizualizate= { "data":getTime(), "utilizatorId":utilizator.id, "filmeId":filme.id }
			await FilmeVizualizate.create(filmevizualizate)
			res.status(201).json({message : 'created'})
		}
		
	} catch (e) {
		console.warn(e.stack)
		res.status(500).json({message : 'server error'})
		}
})


app.get('/utilizator/:id1/filme/:id2/filmevizualizate/', (req, res) => {
    FilmeVizualizate.findAll({
    where: { 
    utilizatorId:req.params.id1,
    filmeId: req.params.id2,
  }
})
    .then(filmevizualizate => res.json(filmevizualizate))
})


//PENTRU FILME NEVIZUALIZATE


app.post('/utilizator/:id1/filme/:id2/filmenevizualizate/', async (req, res) => {
	try {
		let filme = await Filme.findById(req.params.id2)
		let utilizator = await Utilizator.findById(req.params.id1)
	
		if (!utilizator && !filme){
			res.status(404).send({message : 'not found'})
		}
		else{
		    let filmenevizualizate= { "data":getTime(), "utilizatorId":utilizator.id, "filmeId":filme.id }
			await FilmeNevizualizate.create(filmenevizualizate)
			res.status(201).json({message : 'created'})
		}
		
	} catch (e) {
		console.warn(e.stack)
		res.status(500).json({message : 'server error'})
		}
})
	
	
/*	
app.get('/utilizator/:id1/filme/:id2/filmenevizualizate/', async (req, res) => {
	  
		let filme = await Filme.findById(req.params.id, {include : ['filme']})
		let utilizator = await Utilizator.findById(req.params.id1)
		if (!utilizator && !filme){
			res.status(404).send({message : 'not found'})
		}
		else{
			res.status(200).json()
		}
})
*/

app.get('/utilizator/:id1/filme/:id2/filmenevizualizate/', (req, res) => {
    FilmeNevizualizate.findAll({
    where: { 
    utilizatorId:req.params.id1,
    filmeId: req.params.id2,
  }
})
    .then(filmenevizualizate => res.json(filmenevizualizate))
})





//functie pt DATE

function getTime(){
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    
    if (dd < 10) {
      dd = '0' + dd;
    }
    
    if (mm < 10) {
      mm = '0' + mm;
    }
    
    today = yyyy + '-' + mm + '-' + dd;
    return today;
}





//pt server
app.listen(8080);